import * as PIXI from "pixi.js";

const placeYellowPiece = (field) => {
  field.texture = PIXI.Texture.fromLoader("./assets/images/stein_gelb.png");
  field.textureColor = 'yellow';
};

const placeRedPiece = (field) => {
  field.texture = PIXI.Texture.fromLoader("./assets/images/stein_rot.png");
  field.textureColor = 'red';
};

const removePiece = (field) => {
  field.texture = PIXI.Texture.EMPTY;
  field.width = 80;
  field.height = 80;
};

export {placeYellowPiece, placeRedPiece, removePiece}