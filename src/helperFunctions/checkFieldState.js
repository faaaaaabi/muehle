import {playerEnum} from "../types/playerEnum";

const isFieldEmpty = (field) => {
  return field.texture.baseTexture.resource === null;
};

const isPieceOwnedByPlayer = (field, player) => {
  return field.textureColor === playerEnum.properties[player].textureColor;
};

export {isFieldEmpty, isPieceOwnedByPlayer}