class Game {
    constructor() {
        this.field = Array(7).fill(Array(7).fill(-1));
        initField();
    }

    initField() {
        this.field.forEach(row => row.forEach( col => {
            if(row === col || row === 6-col || row === 3 || col === 3){
                this.field[row][col] = 0;
            }
        }));
        this.field[3][3] = -1;
    }

    place(i,j, player) {
        if (this.field[i][j] !== -1){
            this.field[i][j] = player
        }

    }

    remove(i,j) {
        if (this.field[i][j] !== -1){
            this.field[i][j] = 0
        }

    }


}