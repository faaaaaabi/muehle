const playerEnum = {
  red: 0,
  yellow: 1,
  properties: {
    1: {textureColor: "red"},
    2: {textureColor: "yellow"},
  }
};

export { playerEnum }