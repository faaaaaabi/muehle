import * as PIXI from 'pixi.js'
import gameLogic from "./gameLogic";
import {GameControl} from "./GameControl";

// The application will create a renderer using WebGL, if possible,
// with a fallback to a canvas render. It will also setup the ticker
// and the root stage PIXI.Container
const app = new PIXI.Application({width: 900, height: 900});

// The application will create a canvas element for you that you
// can then insert into the DOM
document.body.appendChild(app.view);



// load the texture we need
app.loader
.add('muehlebrett', './assets/images/muhlebrett.png')
.load((loader, resources) => {
  // This creates a texture from a 'bunny.png' image
  const muehlebrett = new PIXI.Sprite(resources.muehlebrett.texture);

  muehlebrett.width = 900;
  muehlebrett.height = 900;

  app.stage.addChild(muehlebrett);

  const gameState = new GameControl(app);

  placeButtons(gameState);


  // Listen for frame updates
  app.ticker.add(() => {
    // each frame we spin the bunny around a bit

  });

});

const onClick = (event) => {
  console.log(event.data.getLocalPosition(app.stage));

};

const placeButtons = (gameState) => {
  const pointList = createPointList();
  pointList
      .map(point => createNewButton(point.x, point.y, gameState))
      .forEach(button => gameState.app.stage.addChild(button));
};

const createPointList = () => {
  const start = 40;
  const difference = 409;

  return[
    createPoint(40,40),
    createPoint(40,449),
    createPoint(40,858),
    createPoint(449,40),
    createPoint(858,40),
    createPoint(449,858),
    createPoint(858,858 ),
    createPoint(858,449),

    createPoint(177,177),
    createPoint(177,449),
    createPoint(177,722),

    createPoint(722,177),
    createPoint(722,449),
    createPoint(722,722),

    createPoint(449,177),
    createPoint(449,722),

    createPoint(312,312),
    createPoint(312,449),
    createPoint(312,586),

    createPoint(449,312),
    createPoint(449,586),

    createPoint(586,312),
    createPoint(586,449),
    createPoint(586,586),
  ];
};

const createPoint = (x,y) => {
  return {x,y};
};

const createNewButton = (x, y, gameState) => {
  const textureButton = PIXI.Texture.EMPTY;
  const button = new PIXI.Sprite(textureButton);
  button.interactive = true;
  button.buttonMode = true;

  button.width = 80;
  button.height = 80;
  button.anchor.x = 0.5;
  button.anchor.y = 0.5;

  button.x = x;
  button.y = y;

  button.on('pointerdown', computeNodeState(button, gameState));
  button.textureColor = null;

  return button
};


const computeNodeState = (button, gameState) => {
  return () => {
    gameLogic(button, gameState);
  }
};



