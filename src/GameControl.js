import * as PIXI from "pixi.js";

export const REDPLAYER = 0;
export const YELLOWPLAYER = 1;

export const STATES = {
    SET: "Steine setzen",
    MOVE: "Steine bewegen",
};

const PLAYERDISPLAY = {
    TEXT: {
        "RED": "Spieler Rot",
        "YELLOW": "Spieler Gelb",
    },
    COLOR: {
        "RED": "red",
        "YELLOW": "yellow"
    }
};

const STYLE = {
    font: 'bold italic 36px Arial',
    fill: 'red',
    strokeThickness: 5,
    dropShadowColor: '#000000',
    dropShadowAngle: Math.PI / 6,
    dropShadowDistance: 6,
    wordWrap: true,
    wordWrapWidth: 440
};

export class GameControl {
    constructor(app) {
        this.app = app;
        this.player = REDPLAYER;
        this.state = STATES.SET;

        this.playerDisplay = this.setPlayerDsiplay();
        this.stateDisplay = this.setGameStateDsiplay()
    }

    changePlayer() {
        if (this.player === REDPLAYER) {
            this.player = YELLOWPLAYER;
            this.playerDisplay.style.fill = PLAYERDISPLAY.COLOR.YELLOW;
            this.playerDisplay.text = PLAYERDISPLAY.TEXT.YELLOW;
        } else if (this.player === YELLOWPLAYER) {
            this.player = REDPLAYER;
            this.playerDisplay.style.fill = PLAYERDISPLAY.COLOR.RED;
            this.playerDisplay.text = PLAYERDISPLAY.TEXT.RED;
        }
    }

    changeState() {
        this.state = STATES.MOVE;
        this.stateDisplay.text = STATES.MOVE
    }

    setPlayerDsiplay() {

        const playerDisplay = new PIXI.Text(PLAYERDISPLAY.TEXT.RED, STYLE);
        playerDisplay.x = 650;
        playerDisplay.y = 0;

        this.app.stage.addChild(playerDisplay);
        return playerDisplay;
    };

    setGameStateDsiplay() {

        const stateDisplay = new PIXI.Text(STATES.SET, STYLE);
        stateDisplay.style.fill = "white";
        stateDisplay.x = 150;
        stateDisplay.y = 0;

        this.app.stage.addChild(stateDisplay);
        return stateDisplay;
    };

}

