import {isFieldEmpty, isPieceOwnedByPlayer} from './helperFunctions/checkFieldState'
import {
  placeRedPiece,
  placeYellowPiece,
  removePiece
} from './helperFunctions/pieceHelper'
import {playerEnum as player} from './types/playerEnum'
import {STATES} from "./GameControl";

let numYellowPiecesSet = 0;
let numRedPiecesSet = 0;
let firstPlayer = player.red;
let currentPlayer = firstPlayer;
const maxPiecesPerPlayer = 9;

function gameLogic(field, gameState) {

  gameState.changePlayer();

  if (areAllPiecesSet(gameState)) {
    console.log("move");
    // movePiece(field);
  } else {
    console.log("set");
    setStone(field);
  }
}

// NonFunctional
function movePiece(field) {
  if(isPieceOwnedByPlayer(field, currentPlayer)) {
    removePiece(field);
  }
}

function setStone(field) {
  if (isFieldEmpty(field)) {
    if (currentPlayer === player.red) {
      placeRedPiece(field);
      togglePlayer();
      incrementNumRedPiecesSet();
    } else {
      placeYellowPiece(field);
      togglePlayer();
      incrementNumYellowPiecesSet();
    }
  }
}

function togglePlayer() {
  if (currentPlayer === player.red) {
    currentPlayer = player.yellow;
    return
  }
  currentPlayer = player.red;
}

function incrementNumYellowPiecesSet() {
  numYellowPiecesSet += 1;
}

function incrementNumRedPiecesSet() {
  numRedPiecesSet += 1;
}

function areAllPiecesSet(gamestate) {

  if (gamestate.state !== STATES.MOVE){

    if(numRedPiecesSet === maxPiecesPerPlayer
        && numYellowPiecesSet === maxPiecesPerPlayer) {
      gamestate.changeState();
      return true;
    }

    return false
  }

  return true;

}

function isCurrentPlayersPiece() {

}

export default gameLogic